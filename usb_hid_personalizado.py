#Instalé la biblioteca hid usando: python -m pip install hid.
#Copié (Windows) el archivo dll bajado de:
#https://github.com/libusb/hidapi/releases/download/hidapi-0.11.0/hidapi-win.zip
#en la carpeta de python.

import hid
import time

#Como uso el dispositivo HID desde un programa, tengo 
#que referenciar usando VID y PID.
vid = 0x0483
pid = 0x5750

#Cuando escribo datos (endpoint OUT) tengo un ID = 1
#y 16 bits de datos (sólo uso el LSB del segundo byte.
led_off = b'\x01\x00\x00'
led_on =  b'\x01\x00\x01'

estado = 0;
#Busco el dispositivo
h = hid.Device(vid,pid)
while True:
    #leo los datos del dispositivo.
    #El primer byte es el ID (2)
    #de los otros dos bytes sólo el LSB del
    #segundo byte está relacionado al estado de B11.
    datos = h.read(3,20)
    if datos[1] == 1:
        break;
    estado = estado ^ 1
    
    if estado == 0:
        h.write(led_off)
    else:
        h.write(led_on);
    print('Led escrito:',estado)
    time.sleep(1)
print()    
print('Apago led')
h.write(led_off)
print('String Fabricante:',h.manufacturer)
print('String Producto:',h.product)
print('String Serie:',h.serial)
